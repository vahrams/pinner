//
//  main.m
//  Pinner
//
//  Created by Vahram Shamtsyan on 7/23/14.
//  Copyright (c) 2014 Pinner. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

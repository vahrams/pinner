//
//  PNPhotoHelper.h
//  Pinner
//
//  Created by vahram on 9/10/14.
//  Copyright (c) 2014 Pinner. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PNTakePhotoDelegate <NSObject>

@required
- (void)usePhoto:(UIImage *)editedImage;



@end

@interface PNPhotoHelper : NSObject

@property (nonatomic, strong)  id <PNTakePhotoDelegate> delegate;

-(void)takePhoto:(UIViewController *)vc;

-(void)choosePhoto:(UIViewController *)vc;

@end

//
//  HCProgressViewController.h
//  HCCoreLibrary
//
//  Created by Tigran Aghababyan on 3/9/11.
//  Copyright 2011 Helix Consulting LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HCProgressViewController : UIViewController

+ (HCProgressViewController*) progressView;

@property (nonatomic) BOOL animate;

- (void)loadAnimation:(NSString*)filenamePattern totalFrames:(NSInteger)frames duration:(CGFloat)duration;

- (void)showInView:(UIView*)aView;
- (void)hide;

@end

//
//  HCProgressViewController.m
//  HCCoreLibrary
//
//  Created by Tigran Aghababyan on 3/9/11.
//  Copyright 2011 Helix Consulting LLC. All rights reserved.
//

#import "HCProgressViewController.h"
#import "HCHelpers.h"

@interface HCProgressViewController()
{
    IBOutlet UIActivityIndicatorView* indicator;
    IBOutlet UIImageView *anumationView;
    
    BOOL _loadedAnimation;
}

@end

@implementation HCProgressViewController
@synthesize animate;

-(void)showView
{
    self.view.hidden = NO;
    self.animate = YES;
}

- (void)showInView:(UIView*)aView
{
    self.view.hidden = NO;
    self.animate = NO;
    [aView addSubview:self.view];
    
    [self performSelector:@selector(showView) withObject:nil afterDelay:0.2];
}

- (void)hide
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showView) object:nil];
    [self.view removeFromSuperview];
    self.view.hidden = YES;
    self.animate = NO;
}


- (void) setAnimate:(BOOL)a
{
    if(a)
    {
        self.view.hidden = NO;
        [anumationView startAnimating];
    }
    else
    {
        [anumationView stopAnimating];
    }
}

- (void)loadAnimation:(NSString*)filenamePattern totalFrames:(NSInteger)framesCount duration:(CGFloat)duration
{
    NSMutableArray* frames = [NSMutableArray new];
    for (int i =0; i < framesCount; ++i)
    {
        UIImage* frame = [UIImage imageNamed:[NSString stringWithFormat:filenamePattern, i]];
        if (frame != nil)
        {
            [frames addObject:frame];
        }
    }
    
    UIImage* img = [anumationView.animationImages objectAtIndex:0];
    
    anumationView.frame = CGRectMake(0, 0, img.size.width, img.size.height);
    anumationView.center = self.view.center;
    
    anumationView.animationImages = frames;
    anumationView.animationDuration = duration;
    anumationView.animationRepeatCount = 0;
    _loadedAnimation = YES;
}

+ (HCProgressViewController*) progressView
{
    return [[self alloc] initWithNibName:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"HCProgressViewController-iPad" : @"HCProgressViewController"
                                  bundle:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    indicator.center = self.view.center;
    anumationView.center = self.view.center;

    
    if (_loadedAnimation)
    {
        indicator.center = self.view.center;
        anumationView.center = self.view.center;
        [indicator stopAnimating];
        indicator.hidden = YES;
        //[anumationView startAnimating];
    }
    else
    {
        [indicator startAnimating];
        indicator.hidden = NO;
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_loadedAnimation)
    {
        [anumationView stopAnimating];
    }
    else
    {
        [indicator stopAnimating];
        indicator.hidden = YES;
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _loadedAnimation = NO;
    }
    return self;
}

- (void)dealloc
{
//    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    anumationView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

//
//  PNProgressViewController.m
//  PNCoreLibrary
//
//  Created by Vahram Shamtsyan on 7/10/14.
//  Copyright 2014 Pinner. All rights reserved.
//

#import "PNProgressViewController.h"

@interface PNProgressViewController()
{
    IBOutlet UIActivityIndicatorView* indicator;
    IBOutlet UIImageView *animationView;
    
    BOOL _loadedAnimation;
}

@end

@implementation PNProgressViewController
@synthesize animate;

-(void)showView
{
    self.view.hidden = NO;
    self.animate = YES;
}

- (void)showInView:(UIView*)aView
{
    self.view.hidden = NO;
    self.animate = NO;
    [aView addSubview:self.view];
    
    [self performSelector:@selector(showView) withObject:nil afterDelay:0.2];
}

- (void)hide
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(showView) object:nil];
    [self.view removeFromSuperview];
    self.view.hidden = YES;
    self.animate = NO;
}


- (void) setAnimate:(BOOL)a
{
    if(a)
    {
        self.view.hidden = NO;
        [animationView startAnimating];
    }
    else
    {
        [animationView stopAnimating];
    }
}

- (void)loadAnimation:(NSString*)filenamePattern totalFrames:(NSInteger)framesCount duration:(CGFloat)duration
{
    NSMutableArray* frames = [NSMutableArray new];
    for (int i =0; i < framesCount; ++i)
    {
        UIImage* frame = [UIImage imageNamed:[NSString stringWithFormat:filenamePattern, i]];
        if (frame != nil)
        {
            [frames addObject:frame];
        }
    }
    
    UIImage* img = [animationView.animationImages objectAtIndex:0];
    
    animationView.frame = CGRectMake(0, 0, img.size.width, img.size.height);
    animationView.center = self.view.center;
    
    animationView.animationImages = frames;
    animationView.animationDuration = duration;
    animationView.animationRepeatCount = 0;
    _loadedAnimation = YES;
}

+ (PNProgressViewController*) progressView
{
    return [[self alloc] initWithNibName:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"PNProgressViewController-iPad" : @"PNProgressViewController"
                                  bundle:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    indicator.center = self.view.center;
    animationView.center = self.view.center;

    
    if (_loadedAnimation)
    {
        indicator.center = self.view.center;
        animationView.center = self.view.center;
        [indicator stopAnimating];
        indicator.hidden = YES;
        //[anumationView startAnimating];
    }
    else
    {
        [indicator startAnimating];
        indicator.hidden = NO;
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_loadedAnimation)
    {
        [animationView stopAnimating];
    }
    else
    {
        [indicator stopAnimating];
        indicator.hidden = YES;
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _loadedAnimation = NO;
    }
    return self;
}

- (void)dealloc
{
//    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    animationView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

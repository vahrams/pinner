//
//  PNProgressViewController.h
//  PNCoreLibrary
//
//  Created by Vahram Shamtsyan on 7/10/14.
//  Copyright 2014 Pinner. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PNProgressViewController : UIViewController

+ (PNProgressViewController*) progressView;

@property (nonatomic) BOOL animate;

- (void)loadAnimation:(NSString*)filenamePattern totalFrames:(NSInteger)frames duration:(CGFloat)duration;

- (void)showInView:(UIView*)aView;
- (void)hide;

@end

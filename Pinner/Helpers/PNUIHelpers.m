//
//  PNUIHelpers.m
//  Pinner
//
//  Created by vahram on 8/18/14.
//  Copyright (c) 2014 Pinner. All rights reserved.
//

#import "PNUIHelpers.h"
#import "PNDefaultVC.h"

@implementation PNUIHelpers

+ (UIView *) newBackButtonWithTarget:(id)target
{
    UIView *btnBg = [[UIView alloc] init];
    btnBg.frame = CGRectMake(5, 20, 60, 45);
    btnBg.backgroundColor = [UIColor clearColor];
    
    UIImageView *btnImageView= [[UIImageView alloc] init];
    btnImageView.frame = CGRectMake(15, 10, 10, 20);
    btnImageView.image = [UIImage imageNamed:@"Back.png"];
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, btnBg.frame.size.width, btnBg.frame.size.height);
    [backBtn addTarget:target action:@selector(backWithAnimation) forControlEvents: UIControlEventTouchUpInside];
    
    [btnBg addSubview:btnImageView];
    [btnBg addSubview:backBtn];
    
    
    return btnBg;
}

+ (UIView *) newMenuButtonWithTarget:(id)target
{
    UIView *btnBg = [[UIView alloc] init];
    btnBg.frame = CGRectMake(5, 20, 60, 45);
    btnBg.backgroundColor = [UIColor clearColor];
    
    UIImageView *btnImageView= [[UIImageView alloc] init];
    btnImageView.frame = CGRectMake(15, 10, 20, 20);
    btnImageView.image = [UIImage imageNamed:@"burger_menu"];
    
    
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    menuBtn.frame = CGRectMake(0, 0, btnBg.frame.size.width, btnBg.frame.size.height);
    [menuBtn addTarget:target action:@selector(openMenu) forControlEvents: UIControlEventTouchUpInside];
    
    [btnBg addSubview:btnImageView];
    [btnBg addSubview:menuBtn];
    
    
    return btnBg;
}



+ (UIView *) newCloseModalButtonWithTarget:(id)target
{
    UIView *btnBg = [[UIView alloc] init];
    btnBg.frame = CGRectMake(5, 20, 60, 45);
    btnBg.backgroundColor = [UIColor clearColor];
    
    UIImageView *btnImageView= [[UIImageView alloc] init];
    btnImageView.frame = CGRectMake(15, 10, 10, 20);
    btnImageView.image = [UIImage imageNamed:@"Back.png"];
    
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, btnBg.frame.size.width, btnBg.frame.size.height);
    [backBtn addTarget:target action:@selector(dismissModalWithAnimation) forControlEvents: UIControlEventTouchUpInside];
    
    [btnBg addSubview:btnImageView];
    [btnBg addSubview:backBtn];
    
    
    return btnBg;
}


@end

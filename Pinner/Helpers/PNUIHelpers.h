//
//  PNUIHelpers.h
//  Pinner
//
//  Created by vahram on 8/18/14.
//  Copyright (c) 2014 Pinner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PNUIHelpers : NSObject

+ (UIView *) newBackButtonWithTarget:(id)target;

+ (UIView *) newCloseModalButtonWithTarget:(id)target;

+ (UIView *) newMenuButtonWithTarget:(id)target;

@end

//
//  PNNavigationVC.m
//  Pinner
//
//  Created by vahram on 9/1/14.
//  Copyright (c) 2014 Pinner. All rights reserved.
//

#import "PNNavigationVC.h"

@interface PNNavigationVC ()

@end

@implementation PNNavigationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

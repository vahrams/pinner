//
//  PNRegistrationVC.m
//  Pinner
//
//  Created by Vahram Shamtsyan on 8/18/14.
//  Copyright (c) 2014 Pinner. All rights reserved.
//

#import "PNRegistrationVC.h"
#import "PNUIHelpers.h"
#import "PNNetworking.h"
#import "ActionSheetPicker.h"


//#import "PNMeetingsVC.h"
//#import "PNGeoMessageVC.h"
//#import "PNHomeVC.h"
//#import "PNSocialActiveVC.h"
//#import "PNInboxVC.h"
//
//#import "RDVTabBarController.h"
//#import "RDVTabBarItem.h"
#import "PNMainVC.h"

const int KB_HEIGHT = 216;

@interface PNRegistrationVC () <UITextFieldDelegate>
{
    CGFloat _screenHeight;
    int _TFMovementDistance;
    BOOL _isTextFieldMoved;
    
    NSString *_gender;
    NSDate *_dob;
}

@property (nonatomic, strong) UIView *slidingView;

@property (nonatomic, strong) UITextField *fNameTF;
@property (nonatomic, strong) UITextField *lNameTF;
@property (nonatomic, strong) UITextField *eMailTF;
@property (nonatomic, strong) UITextField *passwordTF;
@property (nonatomic, strong) UIButton *regBtn;
@property (nonatomic, strong) UILabel *lblGender;
@property (nonatomic, strong) UILabel *lblDob;

@property (nonatomic, strong) RDVTabBarController *tabBarController;


@end

@implementation PNRegistrationVC


- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _isTextFieldMoved = NO;
    CGRect frame = textField.frame;
    
    CGFloat moveSize = _screenHeight - KB_HEIGHT - (frame.origin.y + frame.size.height);
    
    if (moveSize < 0)
    {
        _TFMovementDistance = abs(moveSize) + 5;
        [self animateTextField: textField up: YES];
    }
    
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (_isTextFieldMoved)
    {
        [self animateTextField: textField up: NO];
    }
    
}



- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const float movementDuration = 0.3f;
    
    int distance;
    if (up)
    {
        distance = -_TFMovementDistance;
        _isTextFieldMoved = YES;
    }
    else
    {
        distance = -_TFMovementDistance;
        _isTextFieldMoved = NO;
    }
    
    
    
    int movement = (up ? -_TFMovementDistance : _TFMovementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.slidingView.frame = CGRectOffset(self.slidingView.frame, 0, movement);
    [UIView commitAnimations];
}

- (void) pickGender
{
    NSArray *gender = @[@"", @"male", @"female"];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Gender"
                                            rows:gender
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           
                                           _gender = [(NSString *)selectedValue lowercaseString];
                                           self.lblGender.text = _gender;
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         
                                     }
                                          origin:self.view];
}

- (void) pickDOB
{
    NSDate *selectedDate;
    if (_dob)
    {
        selectedDate = _dob;
    }else
    {
        selectedDate = [NSDate date];
    }
    
    [ActionSheetDatePicker showPickerWithTitle:@"BirthDay"
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:selectedDate
                                     doneBlock:^(ActionSheetDatePicker *picker, NSDate *selectedDate, id origin)
     {
         _dob = selectedDate;
         
         NSDateFormatter *_formater = [[NSDateFormatter alloc]init];
         [_formater setDateFormat:@"dd.MM.yyyy"];

         self.lblDob.text = [_formater stringFromDate:_dob];;
         NSLog(@"dob = %@", selectedDate);
         
     }
                                   cancelBlock:^(ActionSheetDatePicker *picker)
     {
         
     }
                                        origin:self.view];

}

- (void)loadTabbar
{

    [[PNMainVC sharedInstance] loadLoggedInState];
    
}

- (void) doregistration
{
    NSString *fname = self.fNameTF.text;
    NSString *lname = self.lNameTF.text;
    NSString *email = self.eMailTF.text;
    NSString *pass = self.passwordTF.text;
    
    NSMutableDictionary *userProfile = [NSMutableDictionary new];
    userProfile[@"name"] = fname;
    userProfile[@"lastName"] = lname;
    userProfile[@"password"] = pass;
    userProfile[@"email"] = email;
    userProfile[@"photo"] = @"";
    userProfile[@"gender"] = _gender;
    userProfile[@"birthdate"] = @([_dob timeIntervalSince1970]);
    userProfile[@"facebookId"] = @"";
    userProfile[@"username"] = @"Vah";
    userProfile[@"lat"] = [NSNumber numberWithFloat:40.186126];
    userProfile[@"long"] = [NSNumber numberWithFloat:44.524745];
    
    
    [[PNNetworking sharedInstance] createUserProfile:userProfile completion:^(NSString *token, NSError *error)
    {

        if (!error)
        {
            self.appDelegate.keychain[@"session_token"] = token;
            self.appDelegate.sessionToken = token;
            [self loadTabbar];
        }
        else
        {
            NSLog(@"Error occured %@", [error localizedDescription]);
        }
    }];
    

}





- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    _screenHeight = screenBounds.size.height;
    
    BOOL screen4Inch;
    
    if (_screenHeight == 568)
    {
        screen4Inch = YES;
    }
    
    UIImageView *bg = [[UIImageView alloc] init];
    bg.frame = CGRectMake(0, 0, 320, _screenHeight);
    
    self.slidingView = [[UIView alloc] init];
    self.slidingView.frame = CGRectMake(0, 0, 320, _screenHeight);
    self.slidingView.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.2];
    
    self.fNameTF = [[UITextField alloc] init];
    self.lNameTF = [[UITextField alloc] init];
    self.eMailTF = [[UITextField alloc] init];
    self.passwordTF = [[UITextField alloc] init];
    
    UIImageView *genderView = [[UIImageView alloc] init];
    UIImageView *dobView = [[UIImageView alloc] init];
    
    self.regBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIColor *placeholderColor = UIColorFromRGB(mainTFPlaceholderColor);
    UIFont *placeholderFont = [UIFont fontWithName:PNFontReg size:17];
    
    NSDictionary *placeholderAttributesDict = @{NSForegroundColorAttributeName: placeholderColor,
                                                NSFontAttributeName:placeholderFont};
    
    NSString *fNamePlaceholderText = @"First Name";
    NSString *lNamePlaceholderText = @"Last Name";
    NSString *emailPlaceholderText = @"E-MAIL";
    NSString *passwordPlaceholderText = @"Password";
    NSString *genderPlaceholderText = @"Gender";
    NSString *dobPlaceholderText = @"BirthDay";
    
    CGFloat fieldLeftMargin = 20;
    CGFloat fNameY;
    CGFloat fieldHeight;
    CGFloat fieldWidth = 280;
    CGFloat fieldVMargin;
    CGFloat fieldHMargin = 20;
    NSString *fieldBgImg;
    
    CGFloat nextYDiff = fieldHeight + fieldVMargin;
    CGFloat smallFieldWidth = 130;
    
    if (screen4Inch)
    {
        fNameY = 200;
        fieldVMargin = 16;
        fieldHeight = 45;
        fieldBgImg = @""
        bg.image = [UIImage imageNamed:@"login_bg"];
    }
    else
    {
        fNameY = 147;
        fieldVMargin = 14;
        fieldHeight = 42;
    }
    
    CGFloat lNameY = fNameY + nextYDiff;
    CGFloat eMailY = lNameY + nextYDiff;
    CGFloat passwordY = eMailY + nextYDiff;
    CGFloat genderY = passwordY + nextYDiff;
    CGFloat regBtnY = genderY + nextYDiff;
    CGFloat dobX = fieldLeftMargin + smallFieldWidth + fieldHMargin;
    
    self.fNameTF.frame = CGRectMake(fieldLeftMargin, fNameY, fieldWidth, fieldHeight);
    [self.fNameTF setBackground:[UIImage imageNamed:@"field"]];
    self.fNameTF.keyboardType = UIKeyboardTypeDefault;
    self.fNameTF.textAlignment = NSTextAlignmentCenter;
    self.fNameTF.textColor = [UIColor darkGrayColor];
    self.fNameTF.tintColor = UIColorFromRGB(mainBlueColor);
    self.fNameTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.fNameTF.autocorrectionType = UITextAutocapitalizationTypeNone;
    
    if ([self.fNameTF respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        self.fNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:fNamePlaceholderText
                                                                             attributes:placeholderAttributesDict];
    } else {
        self.fNameTF.placeholder = fNamePlaceholderText;
    }
    self.fNameTF.delegate = self;
    
    self.lNameTF.frame = CGRectMake(fieldLeftMargin, lNameY, fieldWidth, fieldHeight);
    [self.lNameTF setBackground:[UIImage imageNamed:@"field"]];
    self.lNameTF.keyboardType = UIKeyboardTypeDefault;
    self.lNameTF.textAlignment = NSTextAlignmentCenter;
    self.lNameTF.textColor = [UIColor darkGrayColor];
    self.lNameTF.tintColor = UIColorFromRGB(mainBlueColor);
    self.lNameTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.lNameTF.autocorrectionType = UITextAutocapitalizationTypeNone;
    if ([self.lNameTF respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        self.lNameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:lNamePlaceholderText
                                                                             attributes:placeholderAttributesDict];
    } else {
        self.lNameTF.placeholder = lNamePlaceholderText;
    }
    self.lNameTF.delegate = self;
    
    self.eMailTF.frame = CGRectMake(fieldLeftMargin, eMailY, fieldWidth, fieldHeight);
    [self.eMailTF setBackground:[UIImage imageNamed:@"field"]];
    self.eMailTF.placeholder = @"E-Mail";
    self.eMailTF.keyboardType = UIKeyboardTypeDefault;
    self.eMailTF.textAlignment = NSTextAlignmentCenter;
    self.eMailTF.textColor = [UIColor darkGrayColor];
    self.eMailTF.tintColor = UIColorFromRGB(mainBlueColor);
    self.eMailTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.eMailTF.autocorrectionType = UITextAutocapitalizationTypeNone;
    
    if ([self.eMailTF respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        self.eMailTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:emailPlaceholderText
                                                                             attributes:placeholderAttributesDict];
    } else {
        self.eMailTF.placeholder = emailPlaceholderText;
    }
    self.eMailTF.delegate = self;
    
    self.passwordTF.frame = CGRectMake(fieldLeftMargin, passwordY, fieldWidth, fieldHeight);
    [self.passwordTF setBackground:[UIImage imageNamed:@"field"]];
    self.passwordTF.placeholder = @"Password";
    self.passwordTF.keyboardType = UIKeyboardTypeDefault;
    self.passwordTF.textAlignment = NSTextAlignmentCenter;
    self.passwordTF.textColor = [UIColor darkGrayColor];
    self.passwordTF.tintColor = UIColorFromRGB(mainBlueColor);
    self.passwordTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.passwordTF.autocorrectionType = UITextAutocapitalizationTypeNone;
    self.passwordTF.secureTextEntry = YES;
    self.passwordTF.delegate = self;
    if ([self.passwordTF respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        self.passwordTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:passwordPlaceholderText
                                                                                attributes:placeholderAttributesDict];
    } else {
        self.passwordTF.placeholder = passwordPlaceholderText;
    }
    
    genderView.frame = CGRectMake(fieldLeftMargin, genderY, smallFieldWidth, fieldHeight);
    genderView.image = [UIImage imageNamed:@"field_small"];
    
    genderView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *genderTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickGender)];
    [genderView addGestureRecognizer:genderTapGestureRecognizer];

    
    self.lblGender = [[UILabel alloc] init];
    self.lblGender.frame = CGRectMake(0, 0, smallFieldWidth, fieldHeight);
    self.lblGender.textColor = placeholderColor;
    self.lblGender.backgroundColor = [UIColor clearColor];
    self.lblGender.font = placeholderFont;
    self.lblGender.text = genderPlaceholderText;
    self.lblGender.textAlignment = NSTextAlignmentCenter;
    [genderView addSubview:self.lblGender];

    dobView.frame = CGRectMake(dobX, genderY, smallFieldWidth, fieldHeight);
    dobView.image = [UIImage imageNamed:@"field_small"];
    dobView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *dobTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickDOB)];
    [dobView addGestureRecognizer:dobTapGestureRecognizer];

    
    self.lblDob = [[UILabel alloc] init];
    self.lblDob.frame = CGRectMake(0, 0, smallFieldWidth, fieldHeight);
    self.lblDob.textColor = placeholderColor;
    self.lblDob.backgroundColor = [UIColor clearColor];
    self.lblDob.font = placeholderFont;
    self.lblDob.text = dobPlaceholderText;
    self.lblDob.textAlignment = NSTextAlignmentCenter;
    
    [dobView addSubview:self.lblDob];

    
    self.regBtn.frame = CGRectMake(20, regBtnY, 280, fieldHeight);
    [self.regBtn setImage:[UIImage imageNamed:@"btn_join"] forState:UIControlStateNormal];
    [self.regBtn addTarget:self action:@selector(doregistration) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:bg];
    
    [self.view addSubview:self.slidingView];
    
    [self.slidingView addSubview:self.fNameTF];
    [self.slidingView addSubview:self.lNameTF];
    [self.slidingView addSubview:self.eMailTF];
    [self.slidingView addSubview:self.passwordTF];
    
    [self.slidingView addSubview:genderView];
    [self.slidingView addSubview:dobView];
    
    [self.slidingView addSubview:self.regBtn];
    
    UIView *backBtn = [PNUIHelpers newBackButtonWithTarget:self];
    [self.view addSubview:backBtn];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

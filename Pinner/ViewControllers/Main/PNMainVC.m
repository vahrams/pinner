//
//  PNMainVC.m
//  Pinner
//
//  Created by vahram on 8/6/14.
//  Copyright (c) 2014 Pinner. All rights reserved.
//

#import "PNMainVC.h"

@interface PNMainVC ()

@end

@implementation PNMainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGFloat screenHeight;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    screenHeight = screenBounds.size.height;
    
    BOOL screen4Inch;
    
    if (screenHeight == 568)
    {
        screen4Inch = YES;
    }
    
    UIImageView *bg = [[UIImageView alloc] init];
    UIImageView *logo = [[UIImageView alloc] init];
    
    UIButton *regBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *fbBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //State Normal
    
    [regBtn setBackgroundImage:[UIImage imageNamed:@"btn_transparent_bg"] forState:UIControlStateNormal];
    [regBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [fbBtn setBackgroundImage:[UIImage imageNamed:@"btn_transparent_bg"] forState:UIControlStateNormal];
    [fbBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [loginBtn setBackgroundImage:[UIImage imageNamed:@"btn_blue_bg"] forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    
    
    //State highlighted
    
    [regBtn setBackgroundImage:[UIImage imageNamed:@"btn_white_bg"] forState:UIControlStateHighlighted];
    [regBtn setTitleColor:UIColorFromRGB(mainBlueColor) forState:UIControlStateHighlighted];
    
    [fbBtn setBackgroundImage:[UIImage imageNamed:@"btn_white_bg"] forState:UIControlStateHighlighted];
    [fbBtn setTitleColor:UIColorFromRGB(mainBlueColor) forState:UIControlStateHighlighted];

    [loginBtn setBackgroundImage:[UIImage imageNamed:@"btn_white_bg"] forState:UIControlStateHighlighted];
    [loginBtn setTitleColor:UIColorFromRGB(mainBlueColor) forState:UIControlStateHighlighted];

    
    //ALL States
    
    [regBtn setTitle:@"Register Now" forState:UIControlStateNormal];
    [fbBtn setTitle:@"Sign In with Facebook" forState:UIControlStateNormal];
    [loginBtn setTitle:@"Sign In" forState:UIControlStateNormal];
    
    if (screen4Inch)
    {
        bg.frame = CGRectMake(0, 0, 320, screenHeight);
        bg.image = [UIImage imageNamed:@"login_bg"];

        logo.frame = CGRectMake(77, 125, 166, 184);
        logo.image = [UIImage imageNamed:@"logo"];
        
        regBtn.frame = CGRectMake(51, 364, 218, 44);
        
        fbBtn.frame = CGRectMake(51, 424, 218, 44);
        
        loginBtn.frame = CGRectMake(51, 484, 218, 44);
        
    }
    else
    {
        bg.frame = CGRectMake(0, 0, 320, screenHeight);
        bg.image = [UIImage imageNamed:@"login_bg"];
        
        logo.frame = CGRectMake(77, 80, 166, 184);
        logo.image = [UIImage imageNamed:@"logo"];

    }
    
    [self.view addSubview:bg];
    [self.view addSubview:logo];
    
    [self.view addSubview:regBtn];
    [self.view addSubview:fbBtn];
    [self.view addSubview:loginBtn];
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
